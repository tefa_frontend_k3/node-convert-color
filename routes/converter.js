const colorController = require('../controller/converter_controller')
const router = require('express').Router()

router.get('/:hex', colorController)

module.exports = router
 