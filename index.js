const express = require("express") 
const bodyParser = require("body-parser") 
const cors = require("cors") 
const app = express()

const converterRoutes = require('.//routes/converter')

app.use(bodyParser.json()) 
app.use(bodyParser.urlencoded({extended:true})) 
app.use(cors())

app.use('/converterColor', converterRoutes)

app.listen(8080, () => {
    console.log("Server run on port 8080");
})